#!/usr/bin/env sh

cmd=$@
if [ -z "$cmd" ]; then
  # Default command is run
  cmd="run"
fi

# Set project dir to an empty directory
export CI_PROJECT_DIR=/tmp/test
mkdir /tmp/test

# Execute analyzer with passed in command (e.g. run). This will cause the certificate file
# pointed to by the DefaultBundlePath variable to be written to disk with the contents of
# the ADDITIONAL_CA_CERT_BUNDLE variable
/analyzer $cmd &>/dev/null

# the previous command will write the certificate file to disk, so we need to ensure that we
# wait for the command to finish before running the ssl-test check, otherwise we'll end up
# with a race condition because the ssl-test check will run before the certificate file has
# been written to disk
wait

# Test that we can connect to the SSL server with a crt signed by the CA in ADDITIONAL_CA_CERT_BUNDLE
/ssl-test
