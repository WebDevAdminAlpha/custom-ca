ARG BASE_IMAGE

FROM golang:1.13.7-alpine AS build
WORKDIR /app
COPY . /app
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ssl-test

FROM $BASE_IMAGE
COPY --from=build /app/ssl-test /ssl-test
COPY test.sh /
CMD /test.sh
